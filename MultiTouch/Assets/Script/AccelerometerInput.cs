﻿using UnityEngine;
using System.Collections;

public class AccelerometerInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		bool hasKeyboard = false;
		float x = 0;
		float z = 0;


		transform.Translate(Input.acceleration.x, 0, -Input.acceleration.z);

		if (Input.GetKey (KeyCode.UpArrow)) {
			print ("Hit up key");
			hasKeyboard = true;
			z += .1f;
			transform.Translate (x, 0, z);
		}
		else if (Input.GetKey (KeyCode.DownArrow)) {
			hasKeyboard = true;
			z += -.1f;
			transform.Translate (x, 0, z);
		}
		else if (Input.GetKey (KeyCode.RightArrow)) {
			hasKeyboard = true;
			x += .1f;
			transform.Translate (x, 0, z);
		}
		else if(Input.GetKey (KeyCode.LeftArrow)){
			hasKeyboard = true;
			x += -.1f;
			transform.Translate (x, 0, z);
		}
	}
}
