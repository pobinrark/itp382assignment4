﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
		bool supportMultiTouch = Input.multiTouchEnabled;
		if(supportMultiTouch == true){
			print ("MultiTouch is enabled");
		}
	}
	
	// Update is called once per frame
	void Update () {
		int nbTouches = Input.touchCount;
		if(nbTouches > 0){
			for(int i = 0; i < nbTouches; i++){
				Touch touch = Input.GetTouch (i);
				if(touch.phase == TouchPhase.Began){
					Ray screenRay = Camera.main.ScreenPointToRay(touch.position);
					RaycastHit hit;
					if(Physics.Raycast(screenRay, out hit)){
						print ("User has tapped an object");
						Destroy (hit.collider.gameObject);
					}
				}
			}
		}
		else if(Input.GetMouseButtonDown(0)){
			Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast (screenRay, out hit)){
				if(hit.collider == transform.collider){
					Destroy(this.gameObject);
				}
			}
		}
	}
}
